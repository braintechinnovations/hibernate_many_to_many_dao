package corso.lez18.HibenateManyToManyDAO;

import java.util.ArrayList;

import org.hibernate.Session;

import corso.lez18.HibenateManyToManyDAO.models.Esame;
import corso.lez18.HibenateManyToManyDAO.models.Studente;
import corso.lez18.HibenateManyToManyDAO.models.crud.EsameDAO;
import corso.lez18.HibenateManyToManyDAO.models.crud.StudenteDAO;
import corso.lez18.HibenateManyToManyDAO.models.db.GestoreSessioni;

public class App 
{
    public static void main( String[] args )
    {

    	StudenteDAO stuDao = new StudenteDAO();
    	EsameDAO esaDao = new EsameDAO();
    	
    	//Semplici inserimenti
//    	Studente mario = new Studente("Mario", "Rossi", "MAT1235");
//    	
//    	Esame fis = new Esame("Fisica I", 6, "2021-06-30");
//    	Esame geo = new Esame("Geometria I", 6, "2021-06-20");
//    	
//    	mario.setElencoEsami(new ArrayList<Esame>());
//    	mario.getElencoEsami().add(geo);
//    	mario.getElencoEsami().add(fis);
//    	
//    	esaDao.insert(geo);
//    	esaDao.insert(fis);
//    	stuDao.insert(mario);
    	
    	
    	//INSERIMENTO Studente ed iscrizione ad esami già presenti sul DB
//    	Studente giovanni = new Studente("Giovanni", "Pace", "MAT1237");
//    	
//    	Esame fis = esaDao.findById(2);
//    	Esame geo = esaDao.findById(1);
//    	
//    	giovanni.setElencoEsami(new ArrayList<Esame>());
//    	giovanni.getElencoEsami().add(geo);
//    	giovanni.getElencoEsami().add(fis);
//    	
////    	esaDao.insert(geo);						//Gli esami esistono già!
////    	esaDao.insert(fis);
//    	stuDao.insert(giovanni);
    	
    	//Penso inversamente, creo un esame ed iscrivo gli studenti
    	Esame mat = new Esame("Matematica I", 9, "2020-07-20");
    	
    	Studente mario = stuDao.findById(1);
    	Studente giovanni = stuDao.findById(3);
    	
    	mat.setElencoStudente(new ArrayList<Studente>());
    	mat.getElencoStudente().add(giovanni);
    	mat.getElencoStudente().add(mario);
    	
    	esaDao.insert(mat);
    }
}
