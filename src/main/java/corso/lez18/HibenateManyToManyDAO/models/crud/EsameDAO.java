package corso.lez18.HibenateManyToManyDAO.models.crud;

import java.util.List;

import org.hibernate.Session;

import corso.lez18.HibenateManyToManyDAO.models.Esame;
import corso.lez18.HibenateManyToManyDAO.models.db.GestoreSessioni;

public class EsameDAO implements Dao<Esame>{

	@Override
	public void insert(Esame t) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();
		
		try {
			sessione.beginTransaction();
			sessione.save(t);
			sessione.getTransaction().commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Esame findById(int id) {
		
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			Esame temp = sessione.get(Esame.class, id);
			
			sessione.getTransaction().commit(); 

			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
		return null;
	}

	@Override
	public List<Esame> findAll() {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			List<Esame> elenco = sessione.createQuery("FROM Esame").list();
			
			sessione.getTransaction().commit(); 
			
			return elenco;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return null;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean delete(int id) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();

			Esame temp = sessione.load(Esame.class, id);
			sessione.delete(temp);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean delete(Esame t) {
		
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			sessione.delete(t);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean update(Esame t) {
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			sessione.update(t);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

}
