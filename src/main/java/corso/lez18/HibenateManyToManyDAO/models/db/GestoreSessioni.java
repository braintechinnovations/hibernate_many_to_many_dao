package corso.lez18.HibenateManyToManyDAO.models.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import corso.lez18.HibenateManyToManyDAO.models.Esame;
import corso.lez18.HibenateManyToManyDAO.models.Studente;

public class GestoreSessioni {

	private static GestoreSessioni ogg_gestore;
	private SessionFactory factory;

	public static GestoreSessioni getIstanza() {
		if(ogg_gestore == null) {
			ogg_gestore = new GestoreSessioni();
		}
		
		return ogg_gestore;
	}
	
	public SessionFactory getFactory() {
		if(factory == null) {
			factory = new Configuration()
					.configure("/resources/hibernate_universita.cfg.xml")
					.addAnnotatedClass(Studente.class)
					.addAnnotatedClass(Esame.class)
					.buildSessionFactory();
		}
		
		return factory;
	}
	
}
