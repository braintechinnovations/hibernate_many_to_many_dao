DROP DATABASE IF EXISTS gestione_universitaria_hibernate;
CREATE DATABASE gestione_universitaria_hibernate;
USE gestione_universitaria_hibernate;

CREATE TABLE studente (
	studenteID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(250),
    cognome VARCHAR(250),
    matricola VARCHAR(20) UNIQUE
);

CREATE TABLE esame(
	esameID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(250),
    crediti INTEGER NOT NULL,
    data_esame DATE
);

CREATE TABLE studente_esame(
	studente_rif INTEGER NOT NULL,
    esame_rif INTEGER NOT NULL,
    FOREIGN KEY (studente_rif) REFERENCES studente(studenteID) ON DELETE CASCADE,
    FOREIGN KEY (esame_rif) REFERENCES esame(esameID) ON DELETE CASCADE,
    UNIQUE(studente_rif, esame_rif)
);